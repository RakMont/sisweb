class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :name_book
      t.string :author_name
      t.date :date_created
      t.string :editorial
      t.integer :price

      t.timestamps
    end
  end
end
