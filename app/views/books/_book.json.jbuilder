json.extract! book, :id, :name_book, :author_name, :date_created, :editorial, :price, :created_at, :updated_at
json.url book_url(book, format: :json)
